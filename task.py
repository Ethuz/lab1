import sys
number = int(sys.argv[1])

print(f'octal: {number:o}, hexadecimal: {number:x}')
